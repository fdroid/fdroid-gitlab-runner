
This gitlab-runner provides two key services:

* building and deploying nightly builds of the website at
  https://staging.f-droid.org
* providing a gitlab-runner that is tailored to running Android
  emulators with KVM access

This can be deployed to prod using: `ansible-playbook -v playbook.yml`


## Testing

This is hooked up to a _Vagrantfile_ and GitLab CI.